/******************************************************************************************
Nguoi tao: SungLB NEO JSP
Muc dich: JS xu ly nghiep vu dang ky/huy goi dich vu khach hang tren Mobifone portal.
******************************************************************************************/
/**************************** Khai bao cac noi dung thong bao theo ngon ngu ****************************/
var service_confirm_register="Bạn có chắc chắn muốn đăng ký không?";
var service_confirm_cancel="Hủy dịch vụ?";
var service_action_workding ="Đang thực hiện...";
var service_bt_agree ="Đồng ý";
var service_bt_cancel ="Đóng lại";
var service_title_cancel ="Hủy dịch vụ";
var service_title_register ="Đăng ký";
var service_not_using="Bạn chưa sử dụng gói dịch vụ nào";
var service_error ="Hệ thống lỗi, vui lòng thử lại sau.";

var service_pk_title = "Gói";
/***********************  End Khai bao cac noi dung thong bao theo ngon ngu ****************************/
var jsObj = null;
jsObj = new Object();

//Bien kiem tra dang thuc hien hay khong:
var isCallService = false;

/*
jsObj.status =0;
jsObj.msg ="Thuc hien thanh cong";
jsObj.service="mca";
jsObj.trace ="1##1";
*/
var userLogin = "";
//Ham tra cuu thong tin user dang nhap vao he thong:
function SMgetUserName(){
	var url_request = "/wps/myportal/public/Service_management/a_Service_management_cm";
	$.ajax({
    type: "POST",
    url: url_request,
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
    async: false, //add this
	success: function(data){
		try{
			var index1 = data.indexOf("<USERNAME_LOGIN>");
			var index2 = data.indexOf("</USERNAME_LOGIN>");
			if (index1>0 && index2>0){
				index1 = index1 + "<USERNAME_LOGIN>".length;
				userLogin = data.substring(index1,index2);
			}
		}catch(err){
			//alert(err);
		};
	}
	});
	return userLogin;
}

function SM_setlanguage(lang) {
	if (lang=="en"){
		service_confirm_register="Are you sure register?";
		service_confirm_cancel="Are you sure cancel?";
		service_action_workding ="Excecuting...";
		service_bt_agree ="I Agree";
		service_bt_cancel ="Close form";
		service_title_cancel ="Cancel Service";
		service_title_register ="Register service";
		service_not_using="There is not service you are using...";
		service_error ="System error, try again please !";
		service_pk_title = "Package";
	}
}
function SWGetURL(url_request,userName){
	if (url_request.indexOf("msisdn")>0){
		url_request=url_request.replace("msisdn","msisdn_ex");
	}
	url_request =url_request.trim();
	url_request=url_request + "&msisdn=" + userName;
	url_request = url_request.replace(/&amp;/g,'&');
	return url_request;
}

//Ham kiem tra goi dich vu nguoi dung> Check xem co goi hay khong:
function SM_checkService(url_request){
	//GetUserlogin:
	SMgetUserName();
	if (userLogin=="") {
		//Chua dang nhap:
		setShowGroupObj('pk_cancel',false);
		return;
	}
	if (url_request==null || url_request=="") return;	
	setShowGroupObj('pk_cancel',false);
	if (isCallService) return;
	isCallService= true;
	url_request = SWGetURL(url_request,userLogin);
	$.ajax({
    type: "POST",
    url: url_request,
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
    async: true, //add this
	success: function(data){
		//Da tra ve du lieu:
		isCallService=false;
		//Doan check du lieu:
		if (data==null || data=="") return;
		
		if (data.indexOf("Invalid Msisdn")>-1) return;
		if (data.indexOf("Invalid Service")>-1) return;
		if (data.indexOf("Invalid Package")>-1) return;
		if (data.indexOf("Action only")>-1) return;
		try{
			//Du lieu tra ve dang Json: Co the la String:
			//alert (data);
			jsObj= JSON.parse(data);
			
			//Chua dang ky goi nao:
			if (jsObj.status==0){
				//Chua su dung dich vu: an cac nut huy dich vu:
				setShowGroupObj('pk_cancel',false);
			}else{
				//Da co dich vu: co 2 truong hop sau:
				//1. Dich vu khong co goi: Cho hien thi nut huy, an nut dang ky.
				//2. Dich vu co goi: Cho hien thi nut dang ky cac goi con lai. An nut dang ky goi dang su dung. Hien hut huy goi dang su dung.
				var listPKCode = SM_getListPackage();
				if (listPKCode==null || listPKCode.length==0) return;
				var num = listPKCode.length;
				//Co nhieu goi:
				var trace ="";
				
				if (jsObj.trace === undefined){
				}else{
					trace = jsObj.trace;
					trace = trace.toUpperCase();
				}
				if (num>=2){
					//Thuc hien tim va an cac nut dang ky cho goi dang su dung:
					//Duyet va tim goi:
					for (var i =0; i< num; i++){
						var pkCode = listPKCode[i]; pkCode = pkCode.toUpperCase();
						if (trace.indexOf(pkCode)>-1){
							
							//Hien thi nut huy goi nay:
							setShowObj("pk_cancel_" + listPKCode[i], true);
							//An nut dang ky goi nay:
							setShowObj("pk_register_" + listPKCode[i], false);
							
							//break;//Thoat vong lap.
						}
					}
				}else{
					//Chi co 1 goi duoc khai bao: Co the la dich vu nhieu hoac dich vu khong co goi:
					var service= "";
					if (jsObj.service === undefined) {
					}else{
						service=jsObj.service; 
						service = service.toUpperCase();
					}
					var pkCode = listPKCode[0]; pkCode = pkCode.toUpperCase();
					if (service == pkCode){
						setShowObj("pk_cancel_" + pkCode, true);
						//An hut dang ky goi nay:
						setShowObj("pk_register_" + pkCode, false);
					}else{
						//Truong hop nhieu goi: Tim xem trong du lieu tra ve co ten goi khong?:
						if (trace.indexOf(pkCode)>-1){
							
							//Hien thi nut huy goi nay:
							setShowObj("pk_cancel_" + pkCode, true);
							//An nut dang ky goi nay:
							setShowObj("pk_register_" + pkCode, false);
							
							//break;//Thoat vong lap.
						}else{
							//Ko tim thay:
						    setShowObj("pk_cancel_" + pkCode, false);
						
						    //Hien thi nut dang ky goi nay:
						    setShowObj("pk_register_" + pkCode, true);
						}
					}
				}
			}
		}catch(err){}
	}
	});
}
//Ham lay ve danh sach cac goi dich vu dang duoc khai bao:
function SM_getListPackage() {
	var pkCode = new Array();
	var bt_name = "pk_register";
	try{
		var listObj = document.getElementsByName("pk_register");
		if (listObj!=null){
			var num = listObj.length;
			for (var i =0; i<num; i++){
				var objId = listObj[i].id;
				objId = objId.replace(bt_name + "_", "");
				pkCode.push(objId);
			}
		}
	}catch(err){};
	return pkCode;
}

//Kiem tra goi dang su dung cua thue bao?:
//Luu y: Co dich vu ko co goi: Vi du Funring,mtalk, cb, gc, fm,yahchat,colorsms : Lam sao biet duoc dv co goi hay khong?
function SM_CheckRegisterMI(obj){
	//An het cac nut huy:
	setShowGroupObj('pk_cancel',false);
	//Xu ly cac nut voi goi hien tai:
	if (obj.trace === undefined) return false;
	var trace = obj.trace;
	//Tim goi dang su dung:
	var key = "KT_DATA_WEB_SUCC";
	var index = trace.indexOf(key);
	var pk_code ="";
	if (index>=0) {
		var value = trace.substring(index + key.length + 1);
		var arr = value.split("|");
		if (arr.length>=1){
			pk_code = arr[0].trim();
		}
	}
	if (pk_code.length>0){
		//Co goi dang su dung:
		var bt_register = "pk_register_";
		var bt_cancel = "pk_cancel_";
		//Hien thi nut huy:
		setShowObj(bt_cancel + pk_code.toUpperCase(),true);
		setShowObj(bt_cancel + pk_code.toLowerCase(),true);
		//An nut dang ky:
		setShowObj(bt_register + pk_code.toUpperCase(),false);
		setShowObj(bt_register + pk_code.toLowerCase(),false);
	}
}
//Ham dang ky goi dich vu:
var sm_form_refresh = false;
//Ham dang ky dich vu:
function SM_register(url_request){
	
	//Kiem tra dieu kien dang nhap:
	SM_CheckUserLogin();
	
	if (url_request==null || url_request=="") return;
	if (isCallService) return;
	isCallService= true;//Dang thuc hien thao tac...
	
	sm_form_refresh = false;
	
	service_show_message(true, service_action_workding);
	
	url_request = SWGetURL(url_request,userLogin);
	
	$.ajax({
    type: "POST",
    url: url_request,
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
    async: true, //add this
	success: function(data){
		isCallService=false;
		//Doan check du lieu:
		if (data==null || data==""){
			service_show_message(true,service_error);
			return;
		}	
		if (data.indexOf("Invalid Service")>-1 || data.indexOf("Invalid Package")>-1 || data.indexOf("Action only")>-1) {
			service_show_message(true,data);
			return;
		}
		try{
			jsObj= JSON.parse(data);
			var msg = jsObj.msg;
			if (jsObj.trace!=null) {
				msg = msg + "("+ jsObj.trace +")";
			}
			service_show_message(true,msg);
			//set reload page:
			sm_form_refresh=true;
			//WS_reloadPage('register');//dang ky
		}catch(err){
			//service_show_message(true, service_error);
			service_show_message(true, data);
		};
	}
	});
	return true;
}
//Ham huy goi dich vu:
function SM_cancel(url_request){	
	if (url_request==null || url_request=="") return;
	
	if (isCallService) return;
	isCallService= true;//Dang thuc hien thao tac...
	sm_form_refresh=false;
	service_show_message(true, service_action_workding);
	//service_show_message(true,"Chuc mung ban da dang ky thanh cong...:" + url);
	url_request = SWGetURL(url_request,userLogin);
	
	$.ajax({
    type: "POST",
    url: url_request,
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
    async: true, //add this
	success: function(data){
		isCallService=false;
		
		//Doan check du lieu:
		if (data==null || data==""){
			service_show_message(true,service_error);
			return;
		}
		if (data.indexOf("Invalid Service")>-1 || data.indexOf("Invalid Package")>-1 || data.indexOf("Action only")>-1) {
			service_show_message(true,data);
			return;
		}
		try{
			jsObj= JSON.parse(data);
			var msg = jsObj.msg;
			if (jsObj.code!=null) {
				//msg = msg + "("+ jsObj.code +")";
				msg = msg + "("+ jsObj.trace +")";
			}
			service_show_message(true,msg);
			sm_form_refresh=true;
			//WS_reloadPage('cancel');//Huy
		}catch(err){
			//service_error
			//service_show_message(true,service_error);
			service_show_message(true,data);
		};
	}
	});
	return true;
}
//Set reload page:
function WS_reloadPage(type){
	setTimeout("location.reload()",1000);
}

function setShowGroupObj(group_name,is_show) {
	var obj = document.getElementsByName(group_name);
	if (obj==null) return;
	var num = obj.length;
	var block = 'none';
	if (is_show) block='';
	for (var i =0; i<num; i++){
		obj[i].style.display = block;
	}
	return true;
}
function setShowObj(id,is_show){
	var obj = document.getElementById(id);
	if (obj==null) return false;
	var block = 'none';
	if (is_show) block='';
	obj.style.display = block;
	return true;
}
//append a object div to o ther object:
function appendObject(obj){
	//Get parent of this object:
	var div_panel_id = "service_panel_form";//service_panel_form
	var objPannel = document.getElementById(div_panel_id);
	if (objPannel!=null){
		$(objPannel).remove();
	}
	//append object:
	if (obj.parentNode!=null){
		//append Ok:
		var new_panel = "<div id='"+ div_panel_id + "' class='service_panel_form'>xin chao...</div>";
		$(obj.parentNode).append(new_panel);
	}
}

//type: register or cancel:
function service_show_form(type,url,cObj){
	//Kiem tra dang nhap:
	SM_CheckUserLogin();
	
	var form_tile =service_confirm_register;
	var onclick = "SM_register(\""+ url+ "\")";
	if (type=="cancel") {
		form_tile=service_confirm_cancel;
		onclick = "SM_cancel(\""+ url+ "\");";
	}
	//Tao noi dung form:
	var form = "";
	//Tieu de:
	form +="<div class='service_form_title'>"+ form_tile + "</div>";
	form +="<div class='service_form_line'></div>";
	form +="<div class='service_form_alert' id='service_form_alert'></div>";
	form +="<div class='service_section_button'><input type='button' value='"+ service_bt_agree +"' class='service_form_button' onclick='"+ onclick +"' /> <input type='button' value='"+ service_bt_cancel +"' class='service_form_button' onclick='service_close_form();' /></div>";
	
	var div_form_id = "div_service_form_confirm";
	
	//An form truoc do:
	var objPannel = document.getElementById(div_form_id);
	if (objPannel!=null){
		$(objPannel).remove();
	}
    var div_form = "<div id='"+ div_form_id +"' class='service_form_confirm'>"+ form +"</div>";
	$(cObj.parentNode).append(div_form);
	$("#" + div_form_id).fadeIn(500);
	return true;
}
function service_show_message(is_show, value){
	var obj = document.getElementById ("service_form_alert");
	if (obj!=null){
		if (is_show){
			obj.style.display="";
		}else{
			obj.style.display="none";
		}
		obj.innerHTML = value;
	}
}
function service_close_form(){
	var div_form_id = "div_service_form_confirm";
	var mask ="service_bg_mas";
	$('#' + mask + ',#' + div_form_id).fadeOut(300, function() {
		$('#' + mask).remove(); $('#' + div_form_id).remove();
	});
	
	if (sm_form_refresh){
		//Refesh lai trang:
		WS_reloadPage('');
	}
	return true;
}

//Kiem tra danh sach dich vu dang su dung: Su dung trong my MobiFone:
function SW_CheckingService(url_request, div_container) {
	
	SM_CheckUserLogin();
	
	if (url_request==null || url_request=="") return;
	url_request = url_request.replace(/&amp;/g,'&');
	
	url_request = SWGetURL(url_request,userLogin);
	
	$.ajax({
    type: "POST",
    url: url_request,
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
    async: true, //add this
	success: function(data){
		//Doan check du lieu:
		if (data==null || data=="") return;
		if (data.indexOf("Invalid Msisdn")>-1) return;
		if (data.indexOf("Invalid Service")>-1) return;
		if (data.indexOf("Invalid Package")>-1) return;
		if (data.indexOf("Action only")>-1) return;
		
		//Map service name ---> service title:
		var arr_service_name = new Array();
		arr_service_name["mi"] = "Mobile Internet";
		arr_service_name["mca"] = "MCA";
		arr_service_name["fc"] = "Fast Connect";
		arr_service_name["gc"] = " CVQT";
		arr_service_name["funring"] = "Funring";
		try{
			jsObj= JSON.parse(data);
			var num = jsObj.length;
			var str_form ="<table class='table table-responsive' id='table-list-svusing'>";
			var url_cancel = "";
			var url_register ="";
			var num_using =0;
			for (var i =0; i <num; i++){
				var obj = jsObj[i];
				var pk_code = "none";
				pk_code=SW_GetpkcodeUsing(obj.service, obj.trace);
				
				var service_title_show = obj.service;
				if (arr_service_name[service_title_show]!=null){
					service_title_show = arr_service_name[service_title_show];
				}else{
					service_title_show = service_title_show.toUpperCase();
				}
				if (pk_code.length>0 && pk_code!="none"){
					service_title_show = service_title_show + " (" + pk_code.toUpperCase() + ")";
				}
				if (obj.status==1) {
					num_using++;
					//Hien thi nut huy:
					url_cancel ="/wps/PA_neo-common-portlet/jsp/services/exec.jsp?service=" + obj.service + "&msisdn=" + obj.msisdn + "&action=0&pkg=" + pk_code;
					str_form +="<tr>";
					var script_cancel = "service_show_form('cancel','"+ url_cancel +"',this);";
					str_form +="<td><strong class='blue'>" + service_title_show + "</strong></td><td onclick=\""+ script_cancel +"\"><a target='' title='' href='#' class='grey'>" + service_title_cancel +" <img src='img/red-close.png'></a></td>";
					str_form +="</tr>";
				}else{
					//Hien thi nut dang ky:
					/*
					url_register ="/wps/PA_neo-common-portlet/jsp/services/exec.jsp?service=" + obj.service + "&msisdn=" + obj.msisdn + "&action=1&pkg=" + pk_code;
					
					str_form +="<tr>";
					var script_register = "/wps/myportal/public/Service_management/service_" + obj.service;
					str_form +="<td><strong class='blue'>" + service_title_show + "</strong></td><td><a target='' title='' href='"+ script_register +"' class='grey'>" + service_title_register +" <img src='img/red-close.png'></a></td>";
					str_form +="</tr>";
					*/
				}
			}
			if (num_using==0){
				str_form ="<table class='table table-responsive'>";
				str_form +="<tr><td>"+ service_not_using +"</td><td>&nbsp;</td></tr>";
			}
			
			str_form +="</table>";
			var obj_show = document.getElementById(div_container);
			if (obj_show!=null) obj_show.innerHTML = str_form;
			
			SW_CheckAllServices("", div_container, num_using);
			
		}catch(err){};
	}
	});
}
//Check all service: Ngay 26.06.2015 SungLB bo sung code check toan bo cac dich vu khac cua thue bao.
function SW_CheckAllServices(url_request, div_container, num_using){
	url_request = "";
	var d = new Date();
	var n = d.getTime();
	url_request = "/wps/PA_neo-common-portlet/jsp/services/service_checkAllSV.jsp?time=" + n;
	url_request = url_request.replace(/&amp;/g,'&');
	url_request = SWGetURL(url_request,userLogin);
	//Check cac dich vu khac cua thue bao:
	//alert(url_request);
	$.ajax({
    type: "POST",
    url: url_request,
	contentType:"application/x-www-form-urlencoded; charset=UTF-8",
    async: true, //add this
	success: function(data){
		//Doan check du lieu:
		if (data==null || data=="") return;
		if (data.indexOf("Invalid Msisdn")>-1) return;
		if (data.indexOf("Invalid Service")>-1) return;
		if (data.indexOf("Invalid Package")>-1) return;
		if (data.indexOf("Action only")>-1) return;
		
		//Map service name ---> service title Other:
		//alert(data);
		var arr_service_name = new Array();
		/*
		arr_service_name["mi"] = "Mobile Internet";
		arr_service_name["mca"] = "MCA";
		arr_service_name["fc"] = "Fast Connect";
		arr_service_name["gc"] = " CVQT";
		arr_service_name["funring"] = "Funring";
		*/
		//arr_service_name["fc"] = "Fast Connect";
		arr_service_name["mtv"] = "Mobile TV";
		arr_service_name["mtalk"] = "mTalk";
		arr_service_name["cb"] = " Call Baring";
		arr_service_name["mclip"] = "mClip";
		arr_service_name["mplus"] = "mPlus";
		arr_service_name["mvoice"] = "mVoice";
		arr_service_name["msport"] = "mSport";
		arr_service_name["mfilm"] = "mFilm";
		
		arr_service_name["mexpress"] = "mExpress";
		arr_service_name["mstatus"] = "mStatus";
		arr_service_name["mwin"] = "mWin";
		arr_service_name["magiccall"] = "Magic Call";
		arr_service_name["mmusic"] = "mMusic";
		
		try{
			jsObj= JSON.parse(data);
			var num = jsObj.length;
			var str_form ="";
			//str_form ="<table class='table table-responsive'>";
			var url_cancel = "";
			var url_register ="";
			//var num_using =0;
			for (var i =0; i <num; i++){
				var obj = jsObj[i];
				var pk_code = "none";
				
				//pk_code=SW_GetpkcodeUsing(obj.service, obj.trace);
				pk_code = obj.pkusing;
				var service_title_show = obj.service;
				if (arr_service_name[service_title_show]!=null){
					service_title_show = arr_service_name[service_title_show];
				}else{
					service_title_show = service_title_show.toUpperCase();
				}
				
				var num_pkg = 1;
				var arr_pkg = new Array();
				if (pk_code.indexOf(",")>0){
					arr_pkg.pk_code.split(",");
					num_pkg = arr_pkg.length;
				}else{
					arr_pkg[0] = pk_code;
				}
				//Duyet danh sach cac goi:
				for (var p =0; p < num_pkg; p++){
					//Hien thi nut huy cho cac goi:
					pk_code = arr_pkg[p];
					if (pk_code==null) continue;
					
					pk_code = pk_code.trim();
					if (pk_code=="") return;
					
					if (pk_code.length>0 && pk_code!="none"){
						service_title_show = service_title_show + " (" + pk_code.toUpperCase() + ")";
					}
					
					if (obj.status==1) {
						num_using++;
						//Hien thi nut huy:
						url_cancel ="/wps/PA_neo-common-portlet/jsp/services/exec.jsp?service=" + obj.service + "&msisdn=" + obj.msisdn + "&action=0&pkg=" + pk_code;
						str_form +="<tr>";
						var script_cancel = "service_show_form('cancel','"+ url_cancel +"',this);";
						str_form +="<td><strong class='blue'>" + service_title_show + "</strong></td><td onclick=\""+ script_cancel +"\"><a target='' title='' href='#' class='grey'>" + service_title_cancel +" <img src='/wps/assets/img/red-close.png'></a></td>";
						str_form +="</tr>";
					}else{
						
						//Hien thi nut dang ky. Tam thoi ko su dung nua.
						/*
						url_register ="/wps/PA_neo-common-portlet/jsp/services/exec.jsp?service=" + obj.service + "&msisdn=" + obj.msisdn + "&action=1&pkg=" + pk_code;
						
						str_form +="<tr>";
						var script_register = "/wps/myportal/public/Service_management/service_" + obj.service;
						str_form +="<td><strong class='blue'>" + service_title_show + "</strong></td><td><a target='' title='' href='"+ script_register +"' class='grey'>" + service_title_register +" <img src='img/red-close.png'></a></td>";
						str_form +="</tr>";
						*/
					}
					
				}//End of for p (list of all package for a service...)
			}
			
			if (num_using==0){
				str_form ="<tr><td>"+ service_not_using +"</td><td>&nbsp;</td></tr>";
			}
			
			$("#table-list-svusing").append(str_form);
		}catch(err){};
	}//End of success !!!
	});
}

//Lay ra ma goi dang su dung: Su dung trong My MobiFone:
function SW_GetpkcodeUsing(service,trace){
	var pk_code ="none";
	if (trace === undefined) return pk_code;
	if (service === undefined) return pk_code;
	try {
		//Get pk_code using from mi:
		if (service =="mi"){
			var key = "KT_DATA_WEB_SUCC";
			var index = trace.indexOf(key);
			if (index>=0) {
				var value = trace.substring(index + key.length + 1);
				var arr = value.split("|");
				if (arr.length>=1){
					pk_code = arr[0].trim();
				}
			}
		}
		//End mi
		//Get pk_code using from mca:
		if (service =="mca"){
			//Hien tai chua tra ra duoc goi dang su dung:
		}
		//End mca
		
		//Dich vu chuyen vung quoc te:
		if (service=="gc"){
			//Hien tai chua tra ra duoc goi dang su dung:
			var key = "CVQT";
			var index = trace.indexOf(key);
			if (index>=0){
				var arr = trace.split(":");
				if (arr.length>=2){
					pk_code = arr[1].trim();
				}
			}
		}
		//End gc
	}catch(e){};
	return pk_code;
}
function SM_CheckUserLogin(){
	//Thuc hien lay thong tin user:
	SMgetUserName();
	if (userLogin==""){
		//alert(location.href):
		window.location.href = "/wps/portal/public/loginportal/";
	}
}
function test_func(){
	jsObj.trace ="1##KT_DATA_WEB_SUCC: M10|17/12/2014 09:02:44|16/01/2015 09:02:43|50|";
	jsObj.service = "mi";
	var pk= SW_GetpkcodeUsing(jsObj.service,jsObj.trace);
	alert(pk);
}

